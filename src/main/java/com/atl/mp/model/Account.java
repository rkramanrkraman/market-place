package com.atl.mp.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Account
{
	@Id
	private Integer id;
	private String cname;
	private String add1;
	private String add2;
	private String city;
	private String zip;
	private String country;
	@OneToMany(targetEntity = Contact.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="account_id", referencedColumnName = "id")
	private List<Contact> contacts = new ArrayList<Contact>();
	
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getCname()
	{
		return cname;
	}

	public void setCname(String cname)
	{
		this.cname = cname;
	}

	public String getAdd1()
	{
		return add1;
	}

	public void setAdd1(String add1)
	{
		this.add1 = add1;
	}

	public String getAdd2()
	{
		return add2;
	}

	public void setAdd2(String add2)
	{
		this.add2 = add2;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public List<Contact> getContacts()
	{
		return contacts;
	}

	public void setContacts(List<Contact> contacts)
	{
		this.contacts = contacts;
	}

	@Override
	public String toString()
	{
		//return "Account [cname=" + cname + ", add1=" + add1 + ", add2=" + add2 + ", city=" + city + ", zip=" + zip
		//		+ ", country=" + country + ", contacts=" + contacts + "]";
		return "";
	}

}
