package com.atl.mp.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Contact
{
	@Id
	private Integer id;
	private String name;
	private String email;
	private String add1;
	private String add2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private Integer account_id;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getAdd1()
	{
		return add1;
	}
	public void setAdd1(String add1)
	{
		this.add1 = add1;
	}
	public String getAdd2()
	{
		return add2;
	}
	public void setAdd2(String add2)
	{
		this.add2 = add2;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getZip()
	{
		return zip;
	}
	public void setZip(String zip)
	{
		this.zip = zip;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	
	
	public Integer getAccount_id()
	{
		return account_id;
	}
	public void setAccount_id(Integer account_id)
	{
		this.account_id = account_id;
	}
	@Override
	public String toString()
	{
		return "Contact [name=" + name + ", email=" + email + ", add1=" + add1 + ", add2=" + add2 + ", city=" + city
				+ ", state=" + state + ", zip=" + zip + ", country=" + country + "]";
	}
}
