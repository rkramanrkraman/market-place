package com.atl.mp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.atl.mp.model.Contact;



public interface ContactRepository extends CrudRepository<Contact, Integer>
{
	// Define interfaces that is automatically implemented by JPA
	
	//The below method will find all Contacts matched by name
	List<Contact> findByName(String name);
	
		
	
	
}