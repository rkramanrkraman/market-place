package com.atl.mp.repository;

import org.springframework.data.repository.CrudRepository;

import com.atl.mp.model.Account;



public interface AccountRepository extends CrudRepository<Account, Integer>
{
	// Define interfaces that is automatically implemented by JPA
	
	
}