package com.atl.mp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atl.mp.model.Account;
import com.atl.mp.model.Contact;
import com.atl.mp.repository.AccountRepository;

@Service
public class AccountService
{
	@Autowired
	private AccountRepository accountRepository;

	public List<Account> getAllAccounts()
	{
		List<Account> accounts = new ArrayList<Account>();
		accountRepository.findAll().forEach(accounts::add);
		return accounts;
	}

	public Optional<Account> getAccount(Integer id)
	{
		return accountRepository.findById(id);
	}

	public List<Contact> getAccountContacts(Integer id)
	{
		Optional<Account> account = accountRepository.findById(id);
		try
		{
			return account.get().getContacts();
		} catch (NoSuchElementException e)
		{
			return null;
		}
	}

	public void addAccount(Account account)
	{
		accountRepository.save(account);
	}

	public void updateAccount(Account account)
	{
		accountRepository.save(account);
	}

	public void deleteAccount(Integer id)
	{
		accountRepository.deleteById(id);
	}

}
