package com.atl.mp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atl.mp.model.Contact;
import com.atl.mp.repository.ContactRepository;

@Service
public class ContactService
{
	@Autowired
	private ContactRepository contactRepository;
	

	public List<Contact> getAllContacts()
	{
		List<Contact> contacts = new ArrayList<Contact>();
		contactRepository.findAll()
		.forEach(contacts::add);
		return contacts;
	}
	
	public Optional<Contact> getContact(Integer id)
	{
		return contactRepository.findById(id);
	}
	
	public void addContact(Contact contact)
	{
		contactRepository.save(contact);
	}

	public void updateContact(Contact contact)
	{
		contactRepository.save(contact);
	}

	public void deleteContact(Integer id)
	{
		contactRepository.deleteById(id);
	}
	
}

