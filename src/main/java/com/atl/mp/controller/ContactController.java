package com.atl.mp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atl.mp.model.Contact;
import com.atl.mp.service.ContactService;

@RestController
public class ContactController
{
	@Autowired
	private ContactService contactService;
	
	@RequestMapping("/contacts")
	public List<Contact> getAllContacts()
	{
		return contactService.getAllContacts();
	}
	
	@RequestMapping("/contacts/{id}")
	public Optional<Contact> getContact(@PathVariable Integer id)
	{
		return contactService.getContact(id);
	}
	
	@RequestMapping(method = RequestMethod.POST,value = "/contacts")
	public void addContact(@RequestBody Contact Contact)
	{
		contactService.addContact(Contact);
	}
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts")
	public void updateContact(@RequestBody Contact Contact)
	{
		contactService.updateContact(Contact);
	}
	@RequestMapping(method = RequestMethod.DELETE,value = "/contacts/{id}")
	public void deleteContact(@PathVariable Integer id)
	{
		contactService.deleteContact(id);
	}
}
