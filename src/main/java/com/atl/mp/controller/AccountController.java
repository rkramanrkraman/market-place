package com.atl.mp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atl.mp.model.Account;
import com.atl.mp.model.Contact;
import com.atl.mp.service.AccountService;

@RestController
public class AccountController
{
	@Autowired
	private AccountService accountService;
	
	@RequestMapping("/accounts")
	public List<Account> getAllAccounts()
	{
		return accountService.getAllAccounts();
	}
	
	@RequestMapping("/accounts/{id}")
	public Optional<Account> getAccount(@PathVariable Integer id)
	{
		return accountService.getAccount(id);
	}

	@RequestMapping("/accounts/{id}/contacts")
	public List<Contact> getAccountContacts(@PathVariable Integer id)
	{
		return accountService.getAccountContacts(id);
	}

	@RequestMapping(method = RequestMethod.POST,value = "/accounts")
	public void addAccount(@RequestBody Account Account)
	{
		accountService.addAccount(Account);
	}
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts")
	public void updateAccount(@RequestBody Account Account)
	{
		accountService.updateAccount(Account);
	}
	@RequestMapping(method = RequestMethod.DELETE,value = "/accounts/{id}")
	public void deleteAccount(@PathVariable Integer id)
	{
		accountService.deleteAccount(id);
	}
}
